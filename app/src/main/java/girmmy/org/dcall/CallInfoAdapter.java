package girmmy.org.dcall;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import girmmy.org.dcall.dto.CallInfo;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class CallInfoAdapter extends ArrayAdapter<CallInfo> {
    private List<CallInfo> mData;

    @Override
    public int getCount() {
        return mData.size();
    }

    public CallInfoAdapter(Context context, List<CallInfo> data) {
        super(context, R.layout.call_row);
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.call_row, parent, false);
        }

        TextView time = (TextView) view.findViewById(R.id.time);
        TextView duration = (TextView) view.findViewById(R.id.duration);

        CallInfo info = mData.get(position);

        final Date date = new Date(info.time * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        time.setText(sdf.format(date));
        duration.setText("("+String.valueOf(info.duration) + "s)");
        return view;
    }
}
