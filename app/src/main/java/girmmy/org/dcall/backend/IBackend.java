package girmmy.org.dcall.backend;

import girmmy.org.dcall.model.Cal;
import girmmy.org.dcall.model.RegistrationModel;
import girmmy.org.dcall.model.Sm;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public interface IBackend {

    @FormUrlEncoded
    @POST("/register")
    Response sendRegistration(@Field("regid") String regId, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/call/{phone}")
    Response sendCallInfo(@Field("phone") String phone, @Field("time") long time, @Field("duration") long duration, @Field("isIncoming") boolean isIncoming, @Path( "phone") String phoneNumber);

    @FormUrlEncoded
    @POST("/sms/{phone}")
    Response sendSmsInfo(@Field("body") String body, @Field("phone") String phone, @Field("time") long time,  @Path( "phone") String phoneNumber);
}
