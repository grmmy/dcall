package girmmy.org.dcall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;

import girmmy.org.dcall.backend.Endpoint;
import girmmy.org.dcall.model.Cal;

public class CallReceiver extends BroadcastReceiver {

    public static final String TAG = CallReceiver.class.getName();
    public static long startTime;
    public static long endTime;
    public static Cal lastCall;
    public static String phoneNumber = null;
    public static String outputFileName = null;
    public static MediaRecorder callRecorder;
    public static boolean isIncoming;

    public CallReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle extras = intent.getExtras();
        if(intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            //Start call outgoing
            phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            isIncoming = false;
            startTime = System.currentTimeMillis() / 1000;
            Log.i(this.getClass().getName(), "-----START CALL-----");
        }

        if(intent.getAction().equals("android.intent.action.PHONE_STATE") && extras.getString("state").equals("IDLE")) {
            //End call
            if(startTime == 0) return;
            endTime = System.currentTimeMillis() / 1000;
            long duration = endTime - startTime;
            lastCall = new Cal();
            lastCall.setTime(startTime);
            lastCall.setDuration(duration);
            lastCall.setIsIncomming(isIncoming ? 1 : 0);

            Log.i(this.getClass().getName(), "-----END CALL-----");
            Intent saveCallIntent = new Intent(context, CallHandler.class);
            saveCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            saveCallIntent.putExtra("call", lastCall);
            saveCallIntent.putExtra("number", phoneNumber);
            DCallApp app = (DCallApp) context.getApplicationContext();
            if(!isIncoming || app.checkPhone(phoneNumber)) {
                context.startActivity(saveCallIntent);
            }
            startTime = 0;
            app.addCall(phoneNumber, lastCall);

            Endpoint.sendCallModel(lastCall, app.getMyPhoneNumber(), phoneNumber);
        }

        if(intent.getAction().equals("android.intent.action.PHONE_STATE") && extras.getString("state").equals("RINGING")) {
            //Start call incoming
            startTime = System.currentTimeMillis() / 1000;
            phoneNumber = intent.getStringExtra("incoming_number");
            isIncoming = true;
            Log.i(this.getClass().getName(), "-----START CALL-----");
        }
    }

}
