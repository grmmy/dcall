package girmmy.org.dcall;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class CallHelper {

    public static void call(Context context, String number) {
        String uri = "tel:"+ number;
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        callIntent.setData(Uri.parse(uri));
        context.startActivity(callIntent);
    }

}
