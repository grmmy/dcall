package girmmy.org.dcall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class PushReceiver extends WakefulBroadcastReceiver {
    private final static String TAG = PushReceiver.class.getName();

    public PushReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        if (!GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            return;
        }
        for (String key : extras.keySet()) {
            Log.i(TAG, key + ": " + String.valueOf(extras.get(key)));
        }

        String number;
        if (extras.containsKey("number")) {
            number = (String) extras.get("number");
        } else {
            number = (String) extras.get("data.number");
        }

        CallHelper.call(context, number);
    }

}
