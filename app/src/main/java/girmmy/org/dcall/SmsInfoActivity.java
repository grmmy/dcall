package girmmy.org.dcall;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import de.greenrobot.event.EventBus;
import girmmy.org.dcall.event.UpdateStatistic;


public class SmsInfoActivity extends ActionBarActivity {

    private String mPhoneNumber = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_info);
        EventBus.getDefault().register(this);
        final Bundle extras = getIntent().getExtras();
        if(extras != null && extras.containsKey("number")) {
            mPhoneNumber = (String) extras.get("number");
        }
        setUpListView();
    }

    public void onEvenMainThread(UpdateStatistic s){
        setUpListView();
    }

    private void setUpListView() {
        ListView smsList = (ListView) findViewById(R.id.sms);
        final DCallApp applicationContext = (DCallApp) getApplicationContext();
        SmsInfoAdapter adapter = new SmsInfoAdapter(this, applicationContext.getSmsInfos(mPhoneNumber));
        smsList.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        setUpListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sms_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
