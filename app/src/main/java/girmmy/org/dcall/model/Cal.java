package girmmy.org.dcall.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "is_incomming",
        "time",
        "duration",
        "path"
})
public class Cal implements Parcelable {

    @JsonProperty("is_incomming")
    private long isIncomming;
    @JsonProperty("time")
    private long time;
    @JsonProperty("duration")
    private long duration;
    @JsonProperty("path")
    private String path;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The isIncomming
     */
    @JsonProperty("is_incomming")
    public long getIsIncomming() {
        return isIncomming;
    }

    /**
     * @param isIncomming The is_incomming
     */
    @JsonProperty("is_incomming")
    public void setIsIncomming(long isIncomming) {
        this.isIncomming = isIncomming;
    }

    /**
     * @return The time
     */
    @JsonProperty("time")
    public long getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    @JsonProperty("time")
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * @return The duration
     */
    @JsonProperty("duration")
    public long getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    @JsonProperty("duration")
    public void setDuration(long duration) {
        this.duration = duration;
    }

    /**
     * @return The path
     */
    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    /**
     * @param path The path
     */
    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.isIncomming);
        dest.writeLong(this.time);
        dest.writeLong(this.duration);
        dest.writeString(this.path);
    }

    public Cal() {
    }

    private Cal(Parcel in) {
        this.isIncomming = in.readLong();
        this.time = in.readLong();
        this.duration = in.readLong();
        this.path = in.readString();
    }

    public static final Parcelable.Creator<Cal> CREATOR = new Parcelable.Creator<Cal>() {
        public Cal createFromParcel(Parcel source) {
            return new Cal(source);
        }

        public Cal[] newArray(int size) {
            return new Cal[size];
        }
    };
}
