package girmmy.org.dcall;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import de.greenrobot.event.EventBus;
import girmmy.org.dcall.event.UpdateStatistic;


public class CallStatic extends ActionBarActivity {

    private String mNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_static);

        final Bundle extras = getIntent().getExtras();
        mNumber = null;
        if(extras != null) {
            mNumber = extras.getString("number");
        }
        EventBus.getDefault().register(this);
        setUpListView();
    }

    private void setUpListView() {
        ListView listView = (ListView) findViewById(R.id.calls);
        DCallApp app = (DCallApp) getApplicationContext();
        CallInfoAdapter adapter = new CallInfoAdapter(this, app.getCallInfo(mNumber));
        listView.setAdapter(adapter);
    }

    public void onEventMainThread(UpdateStatistic s){
        setUpListView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_call_static, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
