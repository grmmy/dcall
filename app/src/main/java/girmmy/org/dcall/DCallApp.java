package girmmy.org.dcall;

import android.app.Application;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryRow;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import girmmy.org.dcall.db.DbHandler;
import girmmy.org.dcall.dto.CallInfo;
import girmmy.org.dcall.dto.ContactInfo;
import girmmy.org.dcall.dto.SmsInfo;
import girmmy.org.dcall.event.UpdateStatistic;
import girmmy.org.dcall.model.Cal;
import girmmy.org.dcall.model.Contacts;
import girmmy.org.dcall.model.Sm;

/**
 * Created by Anton Minashkin on 2/21/15.
 */
public class DCallApp extends Application {

    private static final String TAG = DCallApp.class.getName();
    private Map<String, Contacts> mContacts;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    public String getMyPhoneNumber(){
        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        String number = tm.getLine1Number();
        return number;
    }

    private void saveState() {
        final Database db = DbHandler.getDb(this);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        for (Contacts contact : mContacts.values()) {
            Document dcall = db.getDocument(contact.getPhoneNumber());
            if (dcall == null) {
                dcall = db.createDocument();
            }
            try {
                String json = ow.writeValueAsString(contact);
                Map<String, Object> map = new HashMap<>();
                if (dcall.getCurrentRevision() != null) {
                    map.putAll(dcall.getCurrentRevision().getProperties());
                }
                map.put("_id", contact.getPhoneNumber());
                map.put("body", json);
                dcall.putProperties(map);
            } catch (JsonProcessingException e) {
                Log.e(TAG, "Can't save state to DB", e);
            } catch (IOException e) {
                Log.e(TAG, "Can't save state to DB", e);
            } catch (CouchbaseLiteException e) {
                Log.e(TAG, "Can't save state to DB", e);
            }
        }

        EventBus.getDefault().post(new UpdateStatistic());
    }

    public void addCall(String phoneNumber, Cal call) {
        String checkPhone = fixPhoneNumber(phoneNumber);
        addPhone(checkPhone);
        final Contacts contacts = mContacts.get(checkPhone);
        List<Cal> cals = contacts.getCals();
        if (cals == null) {
            cals = new ArrayList<>();
        }
        cals.add(call);
        contacts.setCals(cals);
        saveState();
    }

    private void init() {
        mContacts = new HashMap<>();
        final Database db = DbHandler.getDb(this);
        final Query contactsQuery = db.getView(DbHandler.CONTACT_VIEW).createQuery();
        final Iterator<QueryRow> result;
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            result = contactsQuery.run();
            for (Iterator<QueryRow> it = result; it.hasNext(); ) {
                QueryRow row = it.next();
                final Contacts contact = new ObjectMapper().readValue(row.getValue().toString(), Contacts.class);
                mContacts.put(contact.getPhoneNumber(), contact);
            }
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Can't read contacts", e);
        } catch (JsonMappingException e) {
            Log.e(TAG, "Can't read contacts", e);
        } catch (JsonParseException e) {
            Log.e(TAG, "Can't read contacts", e);
        } catch (IOException e) {
            Log.e(TAG, "Can't read contacts", e);
        }
    }

    public boolean checkPhone(String phone) {
        return mContacts.containsKey(phone);
    }

    public void addPhone(String phone) {
        String checkPhone = fixPhoneNumber(phone);
        if (!mContacts.containsKey(checkPhone)) {
            Contacts newContact = new Contacts();
            newContact.setPhoneNumber(checkPhone);
            mContacts.put(checkPhone, newContact);
            saveState();
        }
    }

    private String fixPhoneNumber(String phone) {
        String checkPhone = phone;
        if(phone.length() == 10) {
            checkPhone = "+38" + phone;
        }
        return checkPhone;
    }

    public void addSms(String phone, Sm sms) {
        String checkPhone = fixPhoneNumber(phone);
        addPhone(checkPhone);
        final Contacts contacts = mContacts.get(checkPhone);
        List<Sm> smses = contacts.getSms();
        if(smses == null) {
            smses = new ArrayList<>();
        }
        smses.add(sms);
        saveState();
    }

    public List<ContactInfo> getContactsInfo() {
        List<ContactInfo> result = new ArrayList<>();
        for(String key : mContacts.keySet()) {
            Contacts contact = mContacts.get(key);
            int callCount = contact.getCals() == null ? 0 : contact.getCals().size();
            int smsCount = contact.getSms() == null ? 0 : contact.getSms().size();
            ContactInfo info = new ContactInfo(contact.getPhoneNumber(), callCount, smsCount);
            result.add(info);
        }
        return result;
    }

    public List<CallInfo> getCallInfo(String phoneNumber) {
        List<CallInfo> result = new ArrayList<>();

        if(phoneNumber == null) {
            return result;
        }

        if(!mContacts.containsKey(phoneNumber)) {
            return result;
        }

        Contacts contact = mContacts.get(phoneNumber);

        final List<Cal> calls = contact.getCals();
        if(calls == null) {
            return result;
        }
        for(Cal call : calls) {
            result.add(new CallInfo(call.getTime(), call.getDuration(), call.getIsIncomming() == 1));
        }
        return result;
    }

    public List<SmsInfo> getSmsInfos(String phoneNumber) {
        List<SmsInfo> result = new ArrayList<>();
        if(phoneNumber == null) {
            return result;
        }

        if(!mContacts.containsKey(phoneNumber)) {
            return result;
        }

        Contacts contact = mContacts.get(phoneNumber);
        final List<Sm> sms = contact.getSms();
        if(sms == null) {
            return result;
        }

        for (Sm smsItem : sms) {
            result.add(new SmsInfo(smsItem.getBody(), phoneNumber, smsItem.getTime()));
        }

        return result;
    }
}
