package girmmy.org.dcall.backend;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import girmmy.org.dcall.model.Cal;
import girmmy.org.dcall.model.RegistrationModel;
import girmmy.org.dcall.model.Sm;
import retrofit.RestAdapter;
import retrofit.client.Response;

/**
 * Created by Anton Minashkin on 2/21/15.
 */
public class Endpoint {
    private final static String TAG = Endpoint.class.getName();
    private static Executor mExecutor = Executors.newFixedThreadPool(5);
    private static RestAdapter mRestAdapter;
    private static IBackend backend;

    static {
        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.d-call.ngrok.com")
                .build();
        backend = mRestAdapter.create(IBackend.class);
    }


    public static void sendRegistration(RegistrationModel model) {
        mExecutor.execute(() -> {
            final Response response = backend.sendRegistration(model.regId, model.phoneNumber);
            Log.i(TAG, response.toString());
        });
    }

    public static void sendCallModel(Cal call, String phoneNumber, String phoneNumberTo) {
        mExecutor.execute(() -> {
            final Response response = backend.sendCallInfo(phoneNumberTo, call.getTime(), call.getDuration(), call.getIsIncomming() == 1, phoneNumber);
            Log.i(TAG, response.toString());
        });
    }

    public static void sendSmsModel(Sm sms, String phoneNumber, String phoneNumberFrom) {
        mExecutor.execute(() -> {
            sms.setAdditionalProperty("phone", phoneNumberFrom);
            final Response response = backend.sendSmsInfo(sms.getBody(), phoneNumberFrom, sms.getTime(), phoneNumber);
            Log.i(TAG, response.toString());
        });
    }
}
