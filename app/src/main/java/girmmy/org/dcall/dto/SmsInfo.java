package girmmy.org.dcall.dto;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class SmsInfo {
    public final String body;
    public final String phone;
    public final long time;

    public SmsInfo(String body, String phone, long time) {
        this.body = body;
        this.phone = phone;
        this.time = time;
    }
}
