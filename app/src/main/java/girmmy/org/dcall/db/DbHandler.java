package girmmy.org.dcall.db;

import android.content.Context;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import girmmy.org.dcall.model.Contacts;

/**
 * Created by Anton Minashkin on 2/21/15.
 */
public class DbHandler {
    public static final String CONTACT_VIEW = "contacts";

    private final static String TAG = DbHandler.class.getName();
    private static Manager mManager;
    private static Database mDb;

    public synchronized static Database getDb(Context context) {
        if (mDb == null) {
            init(context);
        }
        return mDb;
    }


    private static void init(Context context) {
        // create a manager
        try {
            mManager = new Manager(new AndroidContext(context), Manager.DEFAULT_OPTIONS);
            mDb = mManager.getDatabase("dcall");

            initContactView();
            initCallView();
            initSmsView();

            Log.d(TAG, "Manager created");
        } catch (IOException e) {
            Log.e(TAG, "Cannot create manager object");
            return;
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }


    private static void initSmsView() {
        final View calls = mDb.getView("calls");
        calls.setMap((stringObjectMap, emitter) -> {
            List<Map<String, Object>> contactSms = (List<Map<String, Object>>) stringObjectMap.get("sms");
            final String phoneNumber = (String) stringObjectMap.get("phone_number");
            for(Map<String, Object> sms : contactSms) {
                emitter.emit(phoneNumber, sms);
            }
        }, "1");
    }

    private static void initCallView() {
        final View calls = mDb.getView("calls");
        calls.setMap((stringObjectMap, emitter) -> {
            List<Map<String, Object>> contactCalls = (List<Map<String, Object>>) stringObjectMap.get("calls");
            final String phoneNumber = (String) stringObjectMap.get("phone_number");
            for(Map<String, Object> call : contactCalls) {
                emitter.emit(phoneNumber, call);
            }

        }, "1");
    }

    private static void initContactView() {
        final View contacts = mDb.getView(CONTACT_VIEW);
        contacts.setMap((stringObjectMap, emitter) -> {
            try {
                final Object body = stringObjectMap.get("body");
                if(body == null) return;
                final Contacts contact = new ObjectMapper().readValue(body.toString(), Contacts.class);
                emitter.emit(stringObjectMap.get("phone_number"), body.toString());
            } catch (IOException e) {
                Log.e(TAG, "Can't emit contact", e);
            }
        }, "3");
    }
}
