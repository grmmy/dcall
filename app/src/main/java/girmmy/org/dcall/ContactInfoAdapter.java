package girmmy.org.dcall;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import girmmy.org.dcall.dto.ContactInfo;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class ContactInfoAdapter extends ArrayAdapter<ContactInfo> {
    private final List<ContactInfo> mData;

    public ContactInfoAdapter(Context context, List<ContactInfo> data) {
        super(context, R.layout.contact_row);
        mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = convertView;
        if(view == null){
            view = layoutInflater.inflate(R.layout.contact_row, parent, false);
        }

        TextView contactNumber = (TextView) view.findViewById(R.id.contact_number);
        TextView contactCallCount = (TextView) view.findViewById(R.id.call_count);
        TextView contactSmsCount = (TextView) view.findViewById(R.id.sms_count);


        ContactInfo info = mData.get(position);
        contactNumber.setOnClickListener(v -> CallHelper.call(getContext(), info.phoneNumber));
        contactNumber.setText(info.phoneNumber);

        contactCallCount.setText(toSpannedContent("Call(s) - " + String.valueOf(info.callCount)));
        contactCallCount.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), CallStatic.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("number", info.phoneNumber);
            getContext().startActivity(intent);
        });

        contactSmsCount.setText(toSpannedContent("Sms - " + String.valueOf(info.smsCount)));
        contactSmsCount.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), SmsInfoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("number", info.phoneNumber);
            getContext().startActivity(intent);
        });
        return view;
    }

    private SpannableString toSpannedContent(String content1) {
        SpannableString content = new SpannableString(content1);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }
}
