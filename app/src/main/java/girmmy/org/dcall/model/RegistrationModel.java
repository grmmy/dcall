package girmmy.org.dcall.model;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class RegistrationModel {
    public final String phoneNumber;
    public final String regId;

    public RegistrationModel(String phoneNumber, String regId) {
        this.phoneNumber = phoneNumber;
        this.regId = regId;
    }
}
