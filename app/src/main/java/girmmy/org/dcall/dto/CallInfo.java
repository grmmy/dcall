package girmmy.org.dcall.dto;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class CallInfo {
    public final long time;
    public final long duration;
    public final boolean isIncoming;

    public CallInfo(long time, long duration, boolean isIncoming) {
        this.time = time;
        this.duration = duration;
        this.isIncoming = isIncoming;
    }
}
