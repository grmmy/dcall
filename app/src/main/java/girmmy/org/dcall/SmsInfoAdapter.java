package girmmy.org.dcall;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import girmmy.org.dcall.dto.SmsInfo;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class SmsInfoAdapter extends ArrayAdapter<SmsInfo> {

    private final List<SmsInfo> mData;

    @Override
    public int getCount() {
        return mData.size();
    }

    public SmsInfoAdapter(Context context, List<SmsInfo> data) {
        super(context, R.layout.sms_row);
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if(view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.sms_row, parent, false);
        }

        TextView time = (TextView) view.findViewById(R.id.time);
        TextView body = (TextView) view.findViewById(R.id.body);

        SmsInfo info = mData.get(position);


        final Date date = new Date(info.time * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        time.setText(sdf.format(date));

        body.setText(info.body);

        return view;
    }
}
