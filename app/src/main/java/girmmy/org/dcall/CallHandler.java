package girmmy.org.dcall;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import girmmy.org.dcall.model.Cal;


public class CallHandler extends ActionBarActivity {

    private TextView mDurationView;
    private TextView mPhoneNumberView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_handler);
        mDurationView = (TextView) findViewById(R.id.duration);
        mPhoneNumberView = (TextView) findViewById(R.id.number);
        final Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        if(extras != null) {
            Cal call = extras.getParcelable("call");
            String number = extras.getString("number");
            mDurationView.setText(String.valueOf(call.getDuration()) + "s");
            mPhoneNumberView.setText(number);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_call_handler, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
