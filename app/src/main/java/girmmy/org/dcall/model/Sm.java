package girmmy.org.dcall.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "_id",
        "is_incoming",
        "time",
        "body"
})
public class Sm {

    @JsonProperty("_id")
    private long Id;
    @JsonProperty("is_incoming")
    private long isIncoming;
    @JsonProperty("time")
    private long time;
    @JsonProperty("body")
    private String body;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The Id
     */
    @JsonProperty("_id")
    public long getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    @JsonProperty("_id")
    public void setId(long Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The isIncoming
     */
    @JsonProperty("is_incoming")
    public long getIsIncoming() {
        return isIncoming;
    }

    /**
     *
     * @param isIncoming
     * The is_incoming
     */
    @JsonProperty("is_incoming")
    public void setIsIncoming(long isIncoming) {
        this.isIncoming = isIncoming;
    }

    /**
     *
     * @return
     * The time
     */
    @JsonProperty("time")
    public long getTime() {
        return time;
    }

    /**
     *
     * @param time
     * The time
     */
    @JsonProperty("time")
    public void setTime(long time) {
        this.time = time;
    }

    /**
     *
     * @return
     * The body
     */
    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    /**
     *
     * @param body
     * The body
     */
    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}