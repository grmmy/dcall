package girmmy.org.dcall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import girmmy.org.dcall.backend.Endpoint;
import girmmy.org.dcall.model.Sm;

public class SmsReceiver extends BroadcastReceiver {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String SMS_SENDED = "android.provider.Telephony.SMS_DELIVER";

    private static final String TAG = BroadcastReceiver.class.getName();

    public SmsReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent recieved: " + intent.getAction());

        if (intent.getAction().equals(SMS_RECEIVED)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                final SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                DCallApp app = (DCallApp) context.getApplicationContext();
                if (messages.length > -1) {
                    for (SmsMessage msg : messages) {
                        Log.i(TAG, "Message recieved: " + msg.getMessageBody());
                        String phoneNumber;
                        phoneNumber = msg.getOriginatingAddress();
                        if(app.checkPhone(phoneNumber)) {
                            Sm sms = new Sm();
                            sms.setTime(System.currentTimeMillis() / 1000);
                            sms.setBody(msg.getMessageBody());
                            sms.setIsIncoming(1);
                            app.addSms(phoneNumber, sms);
                            Endpoint.sendSmsModel(sms, app.getMyPhoneNumber(), phoneNumber);
                        }
                    }
                }
            }
        }
    }

}
