package girmmy.org.dcall.dto;

/**
 * Created by Anton Minashkin on 2/22/15.
 */
public class ContactInfo {
    public final String phoneNumber;
    public final int callCount;
    public final int smsCount;

    public ContactInfo(String phoneNumber, int callCount, int smsCount) {
        this.phoneNumber = phoneNumber;
        this.callCount = callCount;
        this.smsCount = smsCount;
    }
}
